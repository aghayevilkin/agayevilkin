package rest_api.agayevilkin.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import rest_api.agayevilkin.dto.request.AuthorRequestDto;
import rest_api.agayevilkin.dto.response.AuthorResponseDto;
import rest_api.agayevilkin.service.AuthorService;

import javax.validation.Valid;


@RestController
@RequestMapping("/author")
@RequiredArgsConstructor
public class AuthorController {

    private final AuthorService authorService;

    @GetMapping("/{id}")
    public AuthorResponseDto getAuthor(@PathVariable("id") Long id) {
        return authorService.findById(id);
    }

    @PostMapping
    public AuthorResponseDto createAuthor(@RequestBody @Valid AuthorRequestDto authorRequestDto) {
        return authorService.createAuthor(authorRequestDto);
    }

    @DeleteMapping("/{id}")
    public void deleteAuthor(@PathVariable("id") Long id) {
        authorService.deleteAuthor(id);
    }

    @PutMapping("/{id}")
    public AuthorResponseDto updateAuthor(@PathVariable("id") Long id, @RequestBody @Valid AuthorRequestDto authorRequestDto) {
        return authorService.update(id, authorRequestDto);
    }
}
