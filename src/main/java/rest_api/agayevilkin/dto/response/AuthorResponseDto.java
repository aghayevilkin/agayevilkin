package rest_api.agayevilkin.dto.response;

import lombok.Data;

@Data
public class AuthorResponseDto {


    private Long id;

    private String firstName;
    private String lastName;


}
