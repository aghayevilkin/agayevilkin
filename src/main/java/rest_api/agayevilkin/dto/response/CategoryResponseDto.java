package rest_api.agayevilkin.dto.response;


import lombok.Data;


@Data
public class CategoryResponseDto {

    private Long id;

    private String name;
    private String description;

}
