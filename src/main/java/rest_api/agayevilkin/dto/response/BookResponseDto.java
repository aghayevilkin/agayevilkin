package rest_api.agayevilkin.dto.response;

import lombok.Data;

@Data
public class BookResponseDto {

    private Long id;

    private String title;
    private String name;
    private String bookDescription;
    private int page;
}
