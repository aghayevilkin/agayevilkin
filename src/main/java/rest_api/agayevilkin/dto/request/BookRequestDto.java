package rest_api.agayevilkin.dto.request;

import com.sun.istack.NotNull;
import lombok.Data;

import javax.validation.constraints.Positive;

@Data
public class BookRequestDto {

    @NotNull
    private String name;

    @NotNull
    private String title;

    @NotNull
    private String bookDescription;

    @NotNull
    @Positive
    private int page;

    private Long authorId;
}
