package rest_api.agayevilkin.dto.request;


import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class CategoryRequestDto {

    @NotNull
    private String name;

    @NotNull
    private String description;

}
