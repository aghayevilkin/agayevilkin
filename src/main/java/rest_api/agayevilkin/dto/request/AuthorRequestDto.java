package rest_api.agayevilkin.dto.request;


import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AuthorRequestDto {

    @NotNull
    private String firstName;

    @NotNull
    private String lastName;


}
