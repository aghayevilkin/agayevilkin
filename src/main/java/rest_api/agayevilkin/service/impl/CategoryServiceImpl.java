package rest_api.agayevilkin.service.impl;

import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import rest_api.agayevilkin.dto.request.CategoryRequestDto;
import rest_api.agayevilkin.dto.response.CategoryResponseDto;
import rest_api.agayevilkin.exception.NotFoundCategoryId;
import rest_api.agayevilkin.model.Category;
import rest_api.agayevilkin.repository.CategoryRepository;
import rest_api.agayevilkin.service.CategoryService;

@Service
@RequiredArgsConstructor
public class CategoryServiceImpl implements CategoryService {

    private final ModelMapper mapper;
    private final CategoryRepository categoryRepository;

    @Override
    public CategoryResponseDto createCategory(CategoryRequestDto categoryRequestDto) {
        Category category = mapper.map(categoryRepository, Category.class);
        return mapper.map(categoryRepository.save(category), CategoryResponseDto.class);
    }

    @Override
    public void deleteCategory(Long id) {
        Category category = categoryRepository.findById(id).orElseThrow(() -> new NotFoundCategoryId(id));
        categoryRepository.delete(category);

    }

    @Override
    public CategoryResponseDto update(Long id, CategoryRequestDto categoryRequestDto) {
        Category category = categoryRepository.findById(id).orElseThrow(() -> new NotFoundCategoryId(id));
        mapper.map(categoryRequestDto,category);
        return mapper.map(categoryRepository.save(category),CategoryResponseDto.class);
    }

    @Override
    public CategoryResponseDto findById(Long id) {
        return categoryRepository.findById(id)
                .map((s) -> mapper.map(s, CategoryResponseDto.class))
                .orElseThrow(() -> new NotFoundCategoryId(id));
    }
}
