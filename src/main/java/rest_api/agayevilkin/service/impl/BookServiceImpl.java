package rest_api.agayevilkin.service.impl;


import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import rest_api.agayevilkin.dto.request.BookRequestDto;
import rest_api.agayevilkin.dto.response.BookResponseDto;
import rest_api.agayevilkin.exception.NotFoundAuthorId;
import rest_api.agayevilkin.exception.NotFoundBookId;
import rest_api.agayevilkin.model.Author;
import rest_api.agayevilkin.model.Book;
import rest_api.agayevilkin.repository.AuthorRepository;
import rest_api.agayevilkin.repository.BookRepository;
import rest_api.agayevilkin.service.BookService;

@Service
@RequiredArgsConstructor
public class BookServiceImpl implements BookService {

    private final BookRepository bookRepository;
    private final AuthorRepository authorRepository;
    private final ModelMapper mapper;

    @Override
    public BookResponseDto createBook(BookRequestDto bookRequestDto) {

        Long authorId = bookRequestDto.getAuthorId();
        Author author = authorRepository.findById(authorId).orElseThrow(() -> new NotFoundAuthorId(authorId));
        Book book = mapper.map(bookRequestDto, Book.class);
        return mapper.map(bookRepository.save(book), BookResponseDto.class);
//        group.setTeacher(teacher);
    }
    @Override
    public void deleteBook(Long id) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new NotFoundBookId(id));
        bookRepository.delete(book);
    }
    @Override
    public BookResponseDto update(Long id, BookRequestDto bookRequestDto) {
        Book book = bookRepository.findById(id).orElseThrow(() -> new NotFoundBookId(id));
        mapper.map(bookRequestDto, book);
        return mapper.map(bookRepository.save(book), BookResponseDto.class);
    }
    @Override
    public BookResponseDto findById(Long id) {
        return bookRepository.findById(id)
                .map((s) -> mapper.map(s, BookResponseDto.class))
                .orElseThrow(() -> new NotFoundBookId(id));
    }
}


/**
 * 1. createBook methodundaki exception Lambda
 * 2. findById method .map lambda
 * 3.
 */