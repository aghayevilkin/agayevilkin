package rest_api.agayevilkin.service.impl;


import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;
import rest_api.agayevilkin.dto.request.AuthorRequestDto;
import rest_api.agayevilkin.dto.response.AuthorResponseDto;
import rest_api.agayevilkin.exception.NotFoundAuthorId;
import rest_api.agayevilkin.model.Author;
import rest_api.agayevilkin.repository.AuthorRepository;
import rest_api.agayevilkin.service.AuthorService;

@Service
@RequiredArgsConstructor
public class AuthorServiceImpl implements AuthorService {

    private final AuthorRepository authorRepository;
    private final ModelMapper mapper;


    @Override
    public AuthorResponseDto createAuthor(AuthorRequestDto authorRequestDto) {
        Author author = mapper.map(authorRequestDto, Author.class);
        return mapper.map(authorRepository.save(author), AuthorResponseDto.class);
    }

    @Override
    public void deleteAuthor(Long id) {
        Author author = authorRepository.findById(id).orElseThrow(() -> new NotFoundAuthorId(id));
        authorRepository.save(author);
    }

    @Override
    public AuthorResponseDto update(Long id, AuthorRequestDto authorRequestDto) {
        Author author = authorRepository.findById(id).orElseThrow(() -> new NotFoundAuthorId(id));
        mapper.map(authorRequestDto,author);
        return mapper.map(authorRepository.save(author),AuthorResponseDto.class);
    }

    @Override
    public AuthorResponseDto findById(Long id) {
        return authorRepository.findById(id)
                .map((s) -> mapper.map(s, AuthorResponseDto.class))
                .orElseThrow(() -> new NotFoundAuthorId(id));
    }
}
