package rest_api.agayevilkin.service;

import rest_api.agayevilkin.dto.request.CategoryRequestDto;
import rest_api.agayevilkin.dto.response.CategoryResponseDto;

public interface CategoryService {
    CategoryResponseDto createCategory(CategoryRequestDto categoryRequestDto);

    void deleteCategory(Long id);

    CategoryResponseDto update (Long id, CategoryRequestDto categoryRequestDto);

    CategoryResponseDto findById(Long id);
}
