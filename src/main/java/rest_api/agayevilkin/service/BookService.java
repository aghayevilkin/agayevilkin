package rest_api.agayevilkin.service;

import org.springframework.stereotype.Service;
import rest_api.agayevilkin.dto.request.BookRequestDto;
import rest_api.agayevilkin.dto.response.BookResponseDto;

@Service
public interface BookService {


    BookResponseDto createBook(BookRequestDto bookRequestDto);

    void deleteBook(Long id);

    BookResponseDto update (Long id, BookRequestDto bookRequestDto);

    BookResponseDto findById(Long id);


}
