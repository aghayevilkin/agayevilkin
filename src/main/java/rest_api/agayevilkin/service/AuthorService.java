package rest_api.agayevilkin.service;


import org.springframework.stereotype.Service;
import rest_api.agayevilkin.dto.request.AuthorRequestDto;
import rest_api.agayevilkin.dto.response.AuthorResponseDto;

@Service
public interface AuthorService {

    AuthorResponseDto createAuthor(AuthorRequestDto authorRequestDto);

    void deleteAuthor(Long id);

    AuthorResponseDto update(Long id, AuthorRequestDto authorRequestDto);

    AuthorResponseDto findById(Long id);

}
