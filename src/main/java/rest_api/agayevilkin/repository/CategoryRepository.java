package rest_api.agayevilkin.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import rest_api.agayevilkin.model.Category;

public interface CategoryRepository extends JpaRepository<Category,Long> {
}
