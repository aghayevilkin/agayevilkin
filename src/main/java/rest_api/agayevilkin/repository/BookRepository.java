package rest_api.agayevilkin.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rest_api.agayevilkin.model.Book;

public interface BookRepository extends JpaRepository<Book, Long> {
}
