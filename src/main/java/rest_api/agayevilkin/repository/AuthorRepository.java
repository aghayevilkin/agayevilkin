package rest_api.agayevilkin.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import rest_api.agayevilkin.model.Author;

public interface AuthorRepository extends JpaRepository<Author,Long> {
}
