package rest_api.agayevilkin.model;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Data
@Table(name = "book")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String title;
    private String name;
    private String bookDescription;
    private int page;

//    @ManyToOne
//    @JoinColumn(name = "category_id")
//    private Category category;
//
//    @Column(name = "status")
//    @Enumerated(EnumType.STRING)
//    private BookStatus status;
//
//    @JsonIgnore
//    @ManyToMany(cascade = CascadeType.PERSIST, fetch = FetchType.LAZY)
//    @JoinTable(
//            name = "book_author",
//            joinColumns = {@JoinColumn(name = "book_id", referencedColumnName = "id")},
//            inverseJoinColumns = {@JoinColumn(name = "author_id", referencedColumnName = "id")})
//    @Builder.Default
//    private Set<Author> authors = new HashSet<>();

}
