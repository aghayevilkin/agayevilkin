package rest_api.agayevilkin.exception;

public class NotFoundAuthorId extends NotFoundException {

    private static final String MESSAGE = "No author with ID %s";

    public NotFoundAuthorId(Long id){
        super(String.format(MESSAGE, id));

    }
}
