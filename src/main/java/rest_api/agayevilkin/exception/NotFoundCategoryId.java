package rest_api.agayevilkin.exception;

public class NotFoundCategoryId extends NotFoundException {
    private static final String MESSAGE = "No category with ID %s";

    public NotFoundCategoryId(Long id){
        super(String.format(MESSAGE, id));

    }
}
