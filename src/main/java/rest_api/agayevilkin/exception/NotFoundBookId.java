package rest_api.agayevilkin.exception;

public class NotFoundBookId extends NotFoundException {

    private static final String MESSAGE = "No book with ID %s";

    public NotFoundBookId(Long id){
        super(String.format(MESSAGE, id));

    }
}
